#ifndef PRESENTATION_H
#define PRESENTATION_H

#include <stdio.h>
#include <stdlib.h>
#include "user.h"
#include "list.h"

void presentation_menu(void);

void presentation_menu_get_options(void);

user_struct read_user_data(void);

void presentation_user(user_struct user);

void presentation_users(list_struct list);


#endif