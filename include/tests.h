#ifndef TESTS_H
#define TESTS_H

#include <stdio.h>
#include <stdlib.h>
#include "../include/user.h"
#include "../include/list.h"

int test_insert_list(void);

int test_insert_many_list(void);

int test_get_list(void);

int test_remove_list_one_element(void);

int test_remove_list_two_elements(void);

int test_update_list(void);

#endif