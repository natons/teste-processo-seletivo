#ifndef LIST_H
#define LIST_H

#include "user.h"

typedef struct List {
    int index;
    user_struct * user;
    struct List * next;
    struct List * previous;
} list_struct;

list_struct * create_list(void);

void insert(list_struct ** list, user_struct ** user);

int remove_list(list_struct ** list, int index);

int update(list_struct ** list, int index, user_struct ** user);

user_struct * get(list_struct ** list, int index);

void destroy_list(list_struct * list);

#endif