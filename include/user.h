#ifndef USER_H
#define USER_H

#include <string.h>

#define T1_WEIGHT 1
#define T2_WEIGHT 2
#define T3_WEIGHT 3

#define NAME_SIZE 50

typedef struct User {
    int registration;
    char name[NAME_SIZE];
    float t1;
    float t2;
    float t3;
    float media;
} user_struct;


user_struct * create_user(int registration, char name[NAME_SIZE], float t1, float t2, float t3);

float calculate_media(float t1, float t2, float t3);

//char * to_json(user_struct * user);

void destroy_user(user_struct * user);

#endif