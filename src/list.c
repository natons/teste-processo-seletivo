#include <stdio.h>
#include <stdlib.h>
#include "../include/list.h"

list_struct * create_list(void){
    list_struct * list = (list_struct *) malloc(sizeof(list_struct));
    list->index = 0;
    list->user = NULL;
    list->next = list;
    list->previous = list;

    return list;
}

void insert(list_struct ** list, user_struct ** user){
    if(list == NULL || *list == NULL || user == NULL || *user == NULL)
        return;

    list_struct * new_list = NULL;
    
    list_struct * list_iteration = *list;
    if(list_iteration->user != NULL)
        while(list_iteration->index < list_iteration->next->index)
            list_iteration = list_iteration->next;
    else {
        (*list)->user = *user;

        return;
    }

    new_list = (list_struct *) malloc(sizeof(list_struct));
    new_list->index = list_iteration->index + 1;
    new_list->user = *user;
    new_list->next = list_iteration->next;
    new_list->previous = list_iteration;

    list_iteration->next = new_list;

    *list = list_iteration->next->next;
}

int remove_list(list_struct ** list, int index){

    if(list == NULL || *list == NULL || index < 0 || (*list)->user == NULL)
        return 0;

    list_struct * list_remove = NULL;
    list_struct * list_iteration = *list;

    if(index == 0 && list_iteration->next != list_iteration){
        list_remove = list_iteration;
        while(list_iteration->index < list_iteration->next->index){
            list_iteration->next->index =  list_iteration->index;
            list_iteration = list_iteration->next;
        }
        list_iteration->next = list_remove->next;
        list_remove->next->previous = list_iteration;

        *list = list_iteration->next;
        destroy_user(list_remove->user);
        destroy_list(list_remove);

        return 1;
    }

    if(list_iteration->next != list_iteration){
        while(list_iteration->index < list_iteration->next->index && list_iteration->index != index)
            list_iteration = list_iteration->next;
        
        if(list_iteration->index == index)
            list_remove = list_iteration;
        else 
            return 0;

    }
    else {
        destroy_user((*list)->user);
        (*list)->index = 0;
        (*list)->user = NULL;
        (*list)->next = *list;
        (*list)->previous = *list;

        return 1;
    }

    
    list_iteration->previous->next = list_iteration->next;
    list_iteration->next->previous = list_iteration->previous;

    list_iteration = list_iteration->next;
    if(list_iteration->next != list_iteration){
        while(list_iteration->index < list_iteration->next->index && list_iteration->index > 0){
            list_iteration->index = list_iteration->index - 1;
            list_iteration = list_iteration->next;
        }
        if(list_iteration->index > 0 ) {
            list_iteration->index = list_iteration->index - 1;
        }
    } else {
        list_iteration->index = 0;
    }

    if(list_iteration->index > 0)
        *list = list_iteration->next;
    else
        *list = list_iteration;
    
    destroy_user(list_remove->user);
    destroy_list(list_remove);

    return 1;
}

int update(list_struct ** list, int index, user_struct ** user){
    if(list == NULL || *list == NULL || index < 0)
        return 0;

    list_struct * list_iteration = *list;
    if(list_iteration->index != list_iteration->next->index){
        while(list_iteration->index < list_iteration->next->index && list_iteration->index != index)
            list_iteration = list_iteration->next;

        if(list_iteration->index == index){
            list_iteration->user = *user;
            return 1;
        }
    } else {
        (*list)->user = *user;
        return 1;
    }
    
    return 0;
}

user_struct * get(list_struct ** list, int index){

    if(list == NULL || *list == NULL || index < 0)
        return NULL;

    list_struct * list_iteration = *list;
    if(list_iteration->user != NULL){
        while(list_iteration->index < list_iteration->next->index && list_iteration->index != index)
            list_iteration = list_iteration->next;

        if(list_iteration->index == index)
            return list_iteration->user;
        else 
            return NULL;

    }

    return NULL;
}

void destroy_list(list_struct * list){
    free(list);
}
