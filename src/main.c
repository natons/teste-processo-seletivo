#include <stdio.h>
#include <stdlib.h>
#include "../include/user.h"
#include "../include/list.h"
#include "../include/tests.h"
#include "../include/presentation.h"

void delay(int time){
    int c, d;
 
   for (c = 1; c <= time; c++)
       for (d = 1; d <= time; d++)
       {}
}

void clear_terminal(void) {
    delay(25000); 
    system("clear");
}

void execute_tests(void) {
    printf("\ntest_insert_list \t\t %s\n", test_insert_list() == 1 ? "pass" : "fail");
    delay(15000); 
    printf("test_insert_many_list \t\t %s\n", test_insert_many_list() == 1 ? "pass" : "fail");
    delay(15000); 
    printf("test_get_list \t\t\t %s\n", test_get_list() == 1 ? "pass" : "fail");
    delay(15000); 
    printf("test_remove_list \t\t %s\n", test_remove_list_one_element() == 1 ? "pass" : "fail");
    delay(15000); 
    printf("test_remove_list_two_elements \t %s\n", test_remove_list_two_elements() == 1 ? "pass" : "fail");
    delay(15000); 
    printf("test_update_list \t\t %s\n\n", test_update_list() == 1 ? "pass" : "fail");
    delay(15000); 
    printf("\t Testes Finalizados\n");
}

void exit_app(void){
    printf("\n\n \t\t Saindo da aplicacao");
    delay(4000);
    printf(".");
    delay(2000);
    printf(".");
    delay(2000);
    printf(".\n\n");
}

int main(){
    int option=0, index;
    list_struct * list = create_list();
    user_struct user_read;
    user_struct * user;

    do {
        user = NULL;
        clear_terminal();
        presentation_menu();
        printf("Digite sua opcao: ");
        scanf("%d", &option);

        switch(option) {
            case 1:
                user_read = read_user_data();
                user = create_user(
                    user_read.registration, user_read.name,
                    user_read.t1, user_read.t2, user_read.t3);
                insert(&list, &user);
                printf("\n\tUsuario inserido com sucesso!\n");
                break;
            case 2:
                printf("Indice: ");
                scanf("%d", &index);
                if(get(&list, index) == NULL){
                    printf("\n\tUsuario nao encontrado\n");
                    break;
                }
                user_read = read_user_data();
                user = create_user(
                    user_read.registration, user_read.name,
                    user_read.t1, user_read.t2, user_read.t3);
                update(&list, index, &user);
                printf("\n\tUsuario atualizado com sucesso!\n");
                break;
            case 3:
                printf("Indice: ");
                scanf("%d", &index);
                if(get(&list, index) == NULL){
                    printf("\nUsuario nao encontrado\n");
                    break;
                }
                remove_list(&list, index);
                printf("\n\tUsuario removido com sucesso!\n");
                break;
            case 4:
                printf("Indice: ");
                scanf("%d", &index);
                user = get(&list, index);
                if(user == NULL){
                    printf("\n\tUsuario nao encontrado\n");
                    break;
                }
                presentation_user(*user);
                printf("\n\tUsuario encontrado!\n");
                break;
            case 5:
                if(list->user == NULL){
                    printf("\n\tLista Vazia!\n");
                    break;
                }
                presentation_users(*list);
                break;
            case 6:
                execute_tests();
                break;
            case 7:
                break;
            default:
                printf("\n\tOpcao Invalida!\n");
                break;
        }
    } while(option != 7);

    exit_app();
    destroy_list(list);
    return 0;
}