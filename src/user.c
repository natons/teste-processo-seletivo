#include <stdio.h>
#include <stdlib.h>
#include "../include/user.h"

user_struct * create_user(int registration, char name[], float t1, float t2, float t3){
    user_struct * user = (user_struct *) malloc(sizeof(user_struct));

    user->registration = registration;
    strcpy(user->name, name);
    user->t1 = t1;
    user->t2 = t2;
    user->t3 = t3;
    user->media = calculate_media(t1,t2,t3);

    return user;
}

float calculate_media(float t1, float t2, float t3){
    return (t1 * T1_WEIGHT + t2 * T2_WEIGHT + t3 * T3_WEIGHT)/(T1_WEIGHT + T2_WEIGHT + T3_WEIGHT);
}

/*char * to_json(user_struct * user){
    return NULL;
}*/

void destroy_user(user_struct * user){
    free(user);
}

