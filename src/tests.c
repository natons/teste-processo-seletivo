#include "../include/tests.h"

int test_insert_list(void){
    list_struct * list_test = create_list();
    user_struct * user_test = create_user(123, "user1", 2.5, 4.8, 9.7);
    insert(&list_test, &user_test);

    int registration = list_test->user->registration; 

    destroy_user(user_test);
    destroy_list(list_test);  
    
    if(registration == 123)      
        return 1;
    
    return 0;
}

int test_insert_many_list(void) {
    list_struct * list_test = create_list();
    user_struct * user_test = create_user(123, "user1", 2.5, 4.8, 9.7);
    user_struct * user_test2 = create_user(124, "user2", 3.5, 2.8, 8.7);
    user_struct * user_test3 = create_user(125, "user3", 4.5, 5.8, 7.0);
    
    insert(&list_test, &user_test);
    insert(&list_test, &user_test2);
    insert(&list_test, &user_test3);

    int registration = get(&list_test, 0)->registration; 
    int registration2 = get(&list_test, 1)->registration;
    int registration3 = get(&list_test, 2)->registration;

    destroy_user(user_test);
    destroy_user(user_test2);
    destroy_user(user_test3);
    destroy_list(list_test);  

    if(registration == 123 && registration2 == 124 && registration3 == 125)      
        return 1;
    
    return 0;
}

int test_get_list(void){
    list_struct * list_test = create_list();
    user_struct * user_test = create_user(123, "user1", 2.5, 4.8, 9.7);
    insert(&list_test, &user_test);

    user_struct * user_get = get(&list_test, 0);
    int registration = user_get != NULL ? user_get->registration : -1;

    destroy_user(user_test);
    destroy_list(list_test);  
    
    if(registration == 123)      
        return 1;
    
    return 0;
}

int test_remove_list_one_element(void){
    list_struct * list_test = create_list();
    user_struct * user_test = create_user(123, "user1", 2.5, 4.8, 9.7);
    insert(&list_test, &user_test);
    remove_list(&list_test, 0);

    user_struct * user_remove = get(&list_test, 0);
    int ret = user_remove == NULL ? 1 : 0;

    return ret;
}

int test_remove_list_two_elements(void){
    list_struct * list_test = create_list();
    user_struct * user_test = create_user(123, "user1", 2.5, 4.8, 9.7);
    user_struct * user_test2 = create_user(124, "user2", 3.5, 2.8, 8.7);
    insert(&list_test, &user_test);
    insert(&list_test, &user_test2);
    remove_list(&list_test, 1);

    list_struct * list_iteration = list_test;
    while(list_iteration->index < list_iteration->next->index){
        if(list_iteration->user->registration == 1234)
            return 0;
        list_iteration = list_iteration->next;
    }
    if(list_iteration->user->registration == 1234)
        return 0;
    
    return 1;
}

int test_update_list(void) {
    list_struct * list_test = create_list();
    user_struct * user_test = create_user(123, "user1", 2.5, 4.8, 9.7);
    user_struct * user_test2 = create_user(124, "user2", 2.5, 4.8, 9.7);
    user_struct * user_test_update = create_user(321, "user_update", 2.5, 4.8, 9.7);
    insert(&list_test, &user_test);
    insert(&list_test, &user_test2);
    update(&list_test, 1, &user_test_update);

    int registration = get(&list_test, 1)->registration;

    destroy_user(user_test);
    destroy_user(user_test2);
    destroy_user(user_test_update);
    destroy_list(list_test);  
    
    if(registration == 321)      
        return 1;
    
    return 0;
}
