#include <stdio.h>
#include <stdlib.h>
#include "../include/user.h"
#include "../include/list.h"

void presentation_menu(void) {
    printf("\t\t Menu de Apresentacao\n");
    printf("1 - Inserir Usuario \t\t 2 - Atualizar Usuario\n");
    printf("3 - Remover Usuario \t\t 4 - Mostrar Usuario\n");
    printf("5 - Mostrar Todos Usuarios \t 6 - Executar Testes\n");
    printf("7 - Sair da Aplicacao\n");
    printf("\n");
}

void presentation_menu_get_options(void){
    printf("\n\n");
    printf("\t\t Opcoes Mostrar Todos Usuarios\n");
    printf("1 - Indice 0 ao Indice final \t\t 2 - Indice final ao Indice 0\n");
}

user_struct read_user_data(void){
    user_struct user;
    float t = 0.0;

    printf("Matricula: ");
    scanf("%d", &user.registration);
    printf("Nome: ");
    scanf("%s", user.name);
    printf("Nota 1: ");
    scanf("%f", &t);
    user.t1 = t;
    printf("Nota 2: ");
    scanf("%f", &t);
    user.t2 = t;
    printf("Nota 3: ");
    scanf("%f", &t);
    user.t3 = t;

    return user;
}

void presentation_user(user_struct user){
    printf("\nMatricula: \t%d\n", user.registration);
    printf("Nome: \t\t%s\n", user.name);
    printf("Nota 1: \t%.2f\n", user.t1);
    printf("Nota 2: \t%.2f\n", user.t2);
    printf("Nota 3: \t%.2f\n", user.t3);
    printf("Media: \t\t%.2f\n", user.media);
}

void presentation_users(list_struct list){
    if(list.next->index != list.index){
        while(list.index < list.next->index) {
            printf("\nIndex: %d", list.index);
            presentation_user(*(list.user));
            printf("\n");
            list = *(list.next);
        }
        printf("\nIndex: %d", list.index);
        presentation_user(*(list.user));
        printf("\n");
    } else {
        printf("\nIndex: %d", list.index);
        presentation_user(*(list.user));
        printf("\n");
    }
}
